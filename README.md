- Dia 12/06 Agil

Vi sobre GIT e GITLAB, como utiliza-los
sobre Readme, e como fazer um Readme para um portifolio no Github

conteudo [aqui] ->
https://gitlab.com/GabrielSilva631/resumo-dia-a-dia/-/issues/1

- Dia 13/06 Agil

Vi sobre Sprint, papeis e responsabilidades, sobre a importancia da comunicacao em equipe
Scrum, Cultura agilvi tambem sobre um guia do Scrum conteudo [aqui] -> https://gitlab.com/GabrielSilva631/resumo-dia-a-dia/-/issues/2

- Dia 14/06 Inicio Rapido em Teste e QA

Carreira em Teste e QA, O perfil do Profissional de Teste e QA, o que se espera desse profissional
o porque precisa de um profissional de Teste e QA, Habilidades Pessoais do Testador/QA: Motivacao
Persistencia, Cusiosidade, Gostar de aprender, Detalhista, Resiliencia e etc...
Habilidades Interpessoais: Ouvir, falar, Ler, Interpretar, saber a maneira de falar com cada pessoa dependendo da pessoa.
Trabalho em equipe: Reunioes, Objetivo, empatia, colaborar com o proximo

- Dia 15/06 continuacao do curso Incio Rapido em Teste QA

Como agir, falar perto de outras pessoas - pensar antes de falar algo para nao lhe prejudicar
profissionalmente - manter o respeito
Se manter atualizado - cuidado com o que postar em redes sociais 
Tomar cuidado ao prestar suporte para alguem - nao entrar em varios sites
Tomar cuidados com conexoes, verificar onde esta se conectando, nao se conectar tanto 
em redes publicas 
Utilizar senhas mais complexas para se protejer de hacker - Verificar o que instala em sua maquina
Tecnicas de teste - Praticas, Processos, Estrategias, Ferramentas

- Dia 16/06 continuacao do curso Incio Rapido em Teste QA

Historia da primeira programadora, testadora, como encontrou o primeiro erro
Quem veio primeiro o Software ou o erro
Bugs - o que sao capaz de gerar, como sera corrigido, testa novamente 
Prejuizo financeiro e de imagem, o prejuizo de imagem seria mais pior, pois seria sua imagem publica que esta em jogo sendo assim as pessoas te olhariam com outros olhos, suspeito
Teste antecipado, o teste deve acontecer o mais breve possivel
Agrupamento de defeitos - Um numero pequeno de modulos contem amaioria dos defeitos descobertos durante o teste antes de sua entrega ou exibe a maioria das falhas operacionais 
Os bugs sao distribuidos de forma heterogenea. Alguns modulos tem mais defeitos do que outros
diefrenca entre Teste e QA, Exemplo de que um QA faz e Aprimorar processos, prevenir reincidencia de erros, melhorar a qualidade dos Sistemas futuros entre outros, ja do tester e basicamente encontrar erros, testar.
Erro - Defeito - Falha 
Pessoas cometem erros (enganos), que produzem defeitos(bugs) no codigo, em um softwarebou sistema ou em um documento.
Teste Ageis e Teste Tradicional 

- Dia 19/06 Fundamentos do teste de software (Back-End)

O porque e importante testar e como isso influencia no ciclo de vida do software.
Ciclo de Desenvolvimento, como funciona cada etapa
Piramide de teste, teste unitario: metodo publico em uma classe, porem pode ser visto como conjunto de teste
Teste integracao: Esste teste tem como objetivo encontrar defeitos e falhas entre varias interfaces de software
Teste E2E: Que se utiliza do inicio ate o final do seu teste
A funcao da piramide e definir niveis de teste para ter uma nocao da quantidade de testes para cada um desses niveis.
Os testes de ponta a ponta sao os que simulam o ambiente real, que abrem navegador, clicam em botoes, preenchem formularios e verifica se aconteceu o que se era esperado.

- Dia 20/06 Ver o conteudo da AWS
Funcao on-premises: administrador de seguranca, responsavel pela instalacao, configuracao, gerenciamento, monitoramento e adocao de solucoes de seguranca.

Funcoes na nuvem: arquiteto enterprise de nuvem
Responsavel por fornecer servicos de nuvem para a empresa
Gerente de programa: responsavel por garantir que a nuvem seja gerenciada adequadamente
Gerente financeiro: Responsavel pelo gerenciamento dos controles financeiros para a nuvem.
Funcao na nuvem: arquiteto de infraestrutura de nuvem - Responsavel por criar as arquiteturas com base nas solucoes de infraestrutrura de nuvem.
Funcao na nuvem: engenheiro de DevOps - Responsavel pela criacao e operacao de fluxos de trabalho rapidos e dimensionaveis.

AWS Cloud Practitioner Essentials (Portuguese)
-Introducao do curso
O que e um modelo cliente-servidor?

Na computacao, um cliente pode ser um navegador da web ou um aplicativo de desktop com o qual uma pessoa interage para fazer solicitacoes a servidores de computador. 

O que e compuntacao em nuvem? E a entrega de recursos de TI sob demanda pela internet com uma definicao de preco de pagamento pelo qual usa.
Modelos de implantacao da computacao em nuvem

Ao selecionar uma estrategia de nuvem, a empresa deve considerar fatores como componentes de aplicativos de nuvem necessarios, ferramentas de gerenciamento de recursos preferenciais e requisitos de infraestrutura de TI legada.
Os tres modelos de implantacao de computacao em nuvem sao computacao baseada na nuvem, local e hibrida: Implantacao baseada na nuvem, Implantacao no local, Implantacao Hibrida
Segue o link para saber mais sobre cada implantacao -> [aqui] https://gitlab.com/GabrielSilva631/resumo-dia-a-dia/-/issues/3

Tipos de intancias do Amazon: Instancias de uso geral equilibram os recursos de computacao, memoria e rede. Voce pode usa-las para diversas cargas de trabalho, como:

servidores de aplicativos
servidores de jogos
servidores de back-end para aplicativos empresariais
bancos de dados pequenos e medios

Instancias otimizadas para computacao

Instancias otimizadas para computacao sao ideais para aplicativos vinculados a computacao que se beneficiam de processadores de alto desempenho. Assim como insancias de uso geral, voce pode usar instancias otimizadas para computacao para cargas de trabalho, como servidores web, de aplicativos e de jogos.

Instancias otimizadas para memoria

sao projetadas para fornecer desempenho rapido para cargas de trabalho que processam grandes conjuntos de dados na memoria.

Instancias de computacao acelerada: usam aceleradores de hardware, ou coprocessadores, para executar algumas funcoes de forma mais eficiente do que e possivel em um software executado em CPUs.

As instancias otimizadas para armazenamento: sao projetadas para cargas de trabalho que exigem alto acesso sequencial de leitura e gravacao a grandes conjuntos de dados no armazenamento local.

Escalabilidade

A escalabilidade envolve comecar apenas com os recursos de que voce precisa e projetar sua arquitetura para responder automaticamente as alteraoces de demanda, fazendo aumentos ou reducoes.
Amazon EC2: O Amazon EC2 Auto Scaling permite que voce adicione ou remova automaticamente instancias do Amazon EC2 em resposta a alteracao da demanda do aplicativo.

Elastic Load Balancing (ELB)

O Elastic Load Balancing e o servico AWS que distribui automaticamente o trafego de entrada de aplicativos entre varios recursos, como instancias do Amazon EC2.
Segue o link abaixo para verificar um exemplo de ELB: https://gitlab.com/GabrielSilva631/resumo-dia-a-dia/-/issues/4

Aplicativos monoliticos e microsservicos 
Os aplicativos sao formados por varios componentes. Os componentes se comunicam entre si para 
transmitir dados, atender solicitacoes e manter o aplicativo em execucao.

Computacao sem servidor: O termo sem servidor significa que o codigo e executado em servidores, sem que voce precise provisionar ou gerenciar esses servidores. Com a computacao sem servidor, voce pode se concentrar na inovacao de novos produtos e recursos em vez de manter servidores.

Exemplo de computacao sem servidor https://gitlab.com/GabrielSilva631/resumo-dia-a-dia/-/issues/5

Infraestrutura Global AWS
Ao determinar a regiao certa para seus servicos, dados e aplicativos, considere os quatro fatores de negocios a seguir:
Conformidade com governanca de dados e requisitos legais, Proximidade com clientes, Servicos disponiveis em uma regiao, Definicao de presos.

- Dia 21/06 Continuacao do conteudo da AWS

Zona de Disponibilidade
Uma Zona de Disponibilidade e um unico data center ou um grupo de data centers em uma Regiao. As Zonas de Disponibilidade estao localizadas a dezenas de quilometros de distancia umas das outras. 

Local de borda
Um local de borda e um site que o Amazon CloudFront usa para armazenar copias em cache do seu conteudo mais proximo dos seus clientes para uma entrega mais rapida.

Maneiras de interagir com os servicos AWS: Console de gerenciamento da AWS, Interface da linha de comando, Kits de desenvolvimento de software

AWS Elastic Beanstalk

Com o AWS Elastic Beanstalk, voce fornece definicoes de codigo e configuracao, e o Elastic Beanstalk implanta os recursos necessarios para executar as seguintes tarefas:

Ajustar capacidade
Balancear carga
Dimensionar de forma automatica
Monitorar a integridade do aplicativo
AWS CloudFormation

Com o AWS CloudFormation, voce pode considerar sua infraestrutura como codigo. Isso significa que voce pode criar um ambiente escrevendo linhas de codigo em vez de usar o AWS Management Console para provisionar recursos individualmente.

Amazon Virtual Private Cloud (Amazon VPC)
O Amazon VPC permite que voce provisione uma secao isolada da nuvem AWS. Nessa secao isolada, voce pode executar os recursos em uma rede virtual que definir. Em uma Virtual Private Cloud (VPC), voce pode organizar seus recursos em sub-redes. Uma sub-rede e uma secao de uma VPC que pode conter recursos como instancias do Amazon EC2.

Gateway da internet
Para permitir que o trafego publico da internet acesse sua VPC, e preciso anexar um gateway da internet a VPC.

Gateway privado virtual
O gateway privado virtual e o componente que permite que o trafego protegido da internet ingresse na VPC. Mesmo que sua conexao com a cafeteria tenha protecao extra, os engarrafamentos sao possiveis porque voce usa o mesmo caminho que outros clientes.

AWS Direct Connect

O AWS Direct Connect e um servico que permite estabelecer uma conexao privada dedicada entre seu data center e uma VPC.

Sub-redes e listas de controle de acesso a rede
Olhando pelo exemplo da cafeteira, os clientes fazem os pedidos ao operador de caixa. O operador de caixa, em seguida, passa os pedidos para o barista. Esse processo permite que a fila prossiga sem problemas a medida que mais clientes entram. 

Suponha que alguns clientes tentem pular a fila do caixa e fazer seus pedidos diretamente ao barista. Isso interrompe o fluxo de trafego e faz com que os clientes acessem uma parte da cafeteria que e restrita a eles.
Sub-redes

Uma sub-rede e uma secao de uma VPC na qual voce pode agrupar recursos com base em necessidades operacionais ou de seguranca. As sub-redes podem ser publicas ou privadas. 

Trafego de rede em uma VPC

Quando um cliente solicita dados de um aplicativo hospedado na nuvem AWS, essa solicitacao e enviada como um pacote. Um pacote e uma unidade de dados enviada pela internet ou por uma rede.

Lista de controle de acesso (ACL) de rede

Uma lista de controle de acesso (ACL) de rede e um firewall virtual que controla o trafego de entrada e saida no nivel de sub-rede.

Filtragem de pacotes stateful

Os grupos de seguranca fazem a filtragem de pacotes stateful. Eles se lembram de decisoes anteriores tomadas para pacotes recebidos.

Armazenamento de instancias
Um armazenamento de instancia e o armazenamento em disco fisicamente anexo ao computador host para uma instancia do EC2 e, portanto, tem a mesma vida util da instancia. Quando a instancia e encerrada, todos os dados no armazenamento de instancias sao perdidos.

Armazenamento de objetos
No armazenamento de objetos, cada objeto consiste em dados, metadados e uma chave.
Os dados podem ser uma imagem, video, documento de texto ou qualquer outro tipo de arquivo. Os metadados contem informacoes sobre o que sao os dados, como eles sao usados, o tamanho do objeto e assim por diante. A chave de um objeto e seu identificador exclusivo.

Armazenamento de arquivos

No armazenamento de arquivos, varios clientes (como usuarios, aplicativos, servidores e assim por diante) podem acessar dados armazenados em pastas de arquivos compartilhadas. Nessa abordagem, um servidor de armazenamento usa armazenamento em bloco com um sistema de arquivos local para organizar os arquivos. Os clientes acessam dados atraves de caminhos de arquivo.


O Amazon Elastic File System (Amazon EFS) e um sistema de arquivos escalavel usado com os servicos de nuvem AWS e recursos locais.

Comparacao entre o Amazon EBS e o Amazon EFS
Um volume do Amazon EBS armazena dados em uma unica Zona de Disponibilidade. 

O Amazon EFS e um servico regional. Ele armazena dados em varias Zonas de Disponibilidade e entre elas.

Bancos de dados relacionais
Em um banco de dados relacional, os dados sao armazenados de forma que se relacionem a outras partes de dados.
Um exemplo de um banco de dados relacional pode ser o sistema de gerenciamento de inventario da cafeteria. Cada registro no banco de dados incluiria dados para um unico item, como nome do produto, tamanho, preco e assim por diante.

Mecanismos de banco de dados do Amazon RDS

O Amazon RDS esta disponivel em seis mecanismos de banco de dados, que otimizam memoria, desempenho ou entrada/saida (E/S). Os mecanismos de banco de dados compativeis sao:

Amazon Aurora
PostgreSQL
MySQL
MariaDB
Oracle Database
Microsoft SQL Server

Amazon Aurora
O Amazon Aurora e um banco de dados relacional de nivel empresarial. E compativel com os bancos de dados relacionais MySQL e PostgreSQL. E ate cinco vezes mais rapido do que os bancos de dados MySQL comuns e ate tres vezes mais rapido do que os bancos de dados PostgreSQL comuns.

Bancos de dados nao relacional

Em um banco de dados nao relacional, voce cria tabelas. Uma tabela e um lugar onde voce pode armazenar e consultar dados.

Bancos de dados nao relacionais sao as vezes referidos como bancos de dados NoSQL porque usam estruturas diferentes de linhas e colunas para organizar dados

Amazon Redshift

O Amazon Redshift é serviço de data warehouse que você pode usar para análise de big data. Ele oferece a capacidade de coletar dados de muitas fontes além de ajudar a entender relações e tendências em todos os seus dados.

AWS Database Migration Service (AWS DMS)

O AWS Database Migration Service (AWS DMS) permite migrar bancos de dados relacionais e não relacionais e outros tipos de armazenamentos de dados.

Com o AWS DMS, você move dados entre bancos de dados de origem e de destino. Os bancos de dados de origem e de destino podem ser do mesmo tipo ou de tipos diferentes. Durante a migração, o banco de dados de origem permanece operacional, reduzindo o tempo de inatividade em qualquer aplicativo que dependa do banco de dados. 

Outros serviços de banco de dados
Amazon DocumentDb, Amazon Neptube, Amazon Quantum Ledger Database (Amazon QLDB)
Amazon Managed Blockchain, Amazon ElastiCache, Amazon DynamoDB Accelerator

AWS Identity and Access Management (IAM)

O AWS Identity and Access Management (IAM) permite que você gerencie o acesso aos serviços e recursos AWS com segurança.
O IAM oferece a flexibilidade de configurar o acesso com base nas necessidades operacionais e de segurança específicas da sua empresa. Você pode fazer isso usando uma combinação dos recursos do IAM, explorados em detalhes nesta lição:

Usuários, grupos e funções do IAM
Políticas do IAM
Autenticação multifator

Usuários do IAM

Um usuário do IAM é uma identidade que você cria na AWS. Ele representa a pessoa ou o aplicativo que interage com os serviços e recursos AWS. Consiste em um nome e credenciais.

Políticas do IAM

Uma políticado IAM é um documento que concede ou nega permissões para serviços e recursos AWS.  

As políticas do IAM permitem que você personalize os níveis de acesso dos usuários aos recursos. Por exemplo, você pode permitir que os usuários acessem todos os buckets do Amazon S3 em sua conta AWS ou apenas um bucket específico.

AWS Organizations

Suponha que sua empresa tenha múltiplas contas AWS. Você pode usar o AWS Organizationspara consolidar e gerenciar múltiplas contas AWS em um local central.

Unidades organizacionais

No AWS Organizations, você pode agrupar contas em unidades organizacionais (UO) para facilitar o gerenciamento de contas com requisitos de negócios ou segurança semelhantes. Ao aplicar uma política a uma UO, todas as contas na UO herdam automaticamente as permissões especificadas na política.  

AWS Artifact

O AWS Artifact é um serviço que fornece acesso sob demanda a relatórios de segurança e conformidade da AWS e a contratos on-line selecionados. O AWS Artifact tem duas seções principais: AWS Artifact Agreements e o AWS Artifact Reports.

Centro de conformidade para o cliente
No Centro de conformidade para o cliente, você pode ler histórias de conformidade dos clientes para descobrir como as empresas de setores regulamentados resolveram vários desafios de conformidade, governança e auditoria.

Ataques de negação de serviço

Um ataque de negação de serviço (DoS) é uma tentativa deliberada de tornar um site ou aplicativo indisponível para os usuários.

Ataques distribuídos de negação de serviço
Essa pessoa e seus amigos telefonam repetidamente para a cafeteria para fazer pedidos, mesmo que não pretendam retirá-los. Esses pedidos são provenientes de números de telefone diferentes e é impossível que a cafeteria bloqueie todos eles. 

AWS Shield

O AWS Shield é um serviço que protege aplicativos contra ataques DDoS. O AWS Shield oferece dois níveis de proteção: Standard e Advanced.

Amazon CloudWatch
O CloudWatch usa métricas para representar os pontos de dados para seus recursos. Os serviços AWS enviam as métricas ao CloudWatch. Em seguida, o CloudWatch usa essas métricas para criar automaticamente gráficos que mostram como o desempenho mudou ao longo do tempo. 

Alarmes do CloudWatch
Com o CloudWatch, você pode criar alarmes que executam ações automaticamente se o valor da métrica ultrapassar ou for inferior a um limite predefinido. 

- Dia 22/06 Continuacao do conteudo da AWS
AWS CloudTrail

O AWS CloudTrail registra as chamadas de API realizadas na sua conta. As informações gravadas são identidade do chamador da API, hora da chamada da API, endereço IP de origem do chamador da API e muito mais. 

CloudTrail Insights

No CloudTrail, você também pode ativar o CloudTrail Insights. Esse recurso opcional permite que o CloudTrail detecte automaticamente atividades de API incomuns em sua conta AWS. 

AWS Trusted Advisor

O Trusted Advisor compara suas descobertas com as práticas recomendadas da AWS em cinco categorias: otimização de custos, desempenho, segurança, tolerância a falhas e limites de serviço. Para as verificações em cada categoria, o Trusted Advisor oferece uma lista de ações recomendadas e recursos adicionais para saber mais sobre as práticas recomendadas da AWS. 

Nível gratuito da AWS

Com o nível gratuito da AWS, você começa a usar determinados serviços sem ter que se preocupar em incorrer em custos durante o período especificado. 

Três tipos de ofertas estão disponíveis: 

Sempre gratuito
12 meses gratuitos
Versão de teste

Cobrança consolidada
O recurso de cobrança consolidada do AWS Organizations permite que você receba uma única fatura para todas as contas AWS de sua organização. Ao consolidar, você pode rastrear facilmente os custos combinados de todas as contas vinculadas em sua organização. O número máximo de contas permitido para uma organização é quatro, mas você pode entrar em contato com o AWS Support para aumentar sua cota, se necessário.

AWS Budgets

No AWS Budgets, você pode criar orçamentos para planejar o uso do serviço, os custos de serviço e as reservas de instâncias.

AWS Cost Explorer

O AWS Cost Explorer é uma ferramenta que permite visualizar, interpretar e gerenciar seus custos e uso da AWS ao longo do tempo.

AWS Support

A AWS oferece quatro planos de suporte diferentes para ajudar você a solucionar problemas, reduzir custos e usar os serviços AWS de forma eficiente. 

Basic

O suporte Basic é gratuito para todos os clientes AWS. Inclui acesso a whitepapers, documentação e comunidades de suporte. 

AWS Marketplace

O AWS Marketplace é um catálogo digital com milhares de ofertas de fornecedores independentes de software. Você pode usar o AWS Marketplace para encontrar, testar e comprar software que pode ser executado na AWS. 

Seis estratégias de migração

Ao migrar aplicativos para a nuvem, seis das estratégias de migração mais comuns que você pode implementar são:

Redefinição de hospedagem
Redefinição de plataforma
Refatoração/rearquitetura
Recompra
Retenção
Inativação

Membros da AWS Snow Family

A AWS Snow Family é uma coleção de dispositivos físicos para transporte físico de até exabytes de dados para dentro e para fora da AWS. 
A AWS Snow Family consiste nos serviços AWS Snowcone, AWS Snowball e AWS Snowmobile.
Esses dispositivos oferecem diferentes pontos de capacidade e a maioria inclui recursos de computação integrados. A AWS é a proprietária e responsável pelo gerenciamento da Snow Family, que integra recursos de segurança, monitoramento, gerenciamento de armazenamento e computação da AWS.  


